<?php

App::uses('AppController', 'Controller');
class DonationsController extends AppController {
  public function get_donations($website_id){
    $donations = $this->Donation->find('all',array(
      'conditions'=>array(
        'Donation.website_id'=>$website_id
      ),
      'limit'=>5
    ));
    if(!empty($donations)){
      return $donations;
    }
    return false;
  }

  public function sum_website_donations($website_id){
    $donations = $this->Donation->find('all',array(
      'conditions'=>array(
        'Donation.website_id'=>$website_id
      ),
      'fields'=>array('Donation.price')
    ));
    $results = array();
    foreach ($donations as $donation){
      $results[] = $donation['Donation']['price'];
    }
    $result = number_format(array_sum($results));
    return $result;
  }
}
?>
