<?php

App::uses('AppController', 'Controller');
class InvitesController extends AppController {
  public function get_invites($website_id){
    $invites = $this->Invite->find('all',array(
      'conditions'=>array(
        'Invite.website_id'=>$website_id
      ),
      'limit'=>5
    ));
    if(!empty($invites)){
      return $invites;
    }
    return false;
  }

  public function count_website_invites($website_id){
    $this->Invite->recursive = -1;
    $invites = $this->Invite->find('count',array(
      'conditions'=>array(
        'Invite.website_id'=>$website_id
      )
    ));
    return $invites;
  }
}
?>
