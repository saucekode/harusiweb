<?php
class SubscribersController extends AppController {

  public function check_active_subscription($website_id){
    $this->Subscriber->recursive = -1;
    $subscription = $this->Subscriber->findByWebsiteId($website_id);
    if(!empty($subscription)){
      if($subscription['Subscriber']['active'] == 1){
        return true;
      }
    }
    return false;
  }

  public function get_subscriber_details($website_id){
    $this->Subscriber->recursive = 1;
    $subscription = $this->Subscriber->findByWebsiteId($website_id);
    return $subscription;
  }

  public function pay_subscription($website_id){

  }
}
?>
