<?php
class AlbumsController extends AppController {

  public function my_album(){
    $website_id = $this->Session->read('Auth.User.Website.id');
    if(empty($website_id)){
      $this->redirect('/dashboard');
    }
    $this->set(compact('website_id'));

    $this->Album->recursive = 0;
    $albums = $this->Album->find('all',array(
      'conditions'=>array(
        'Album.website_id'=>$website_id,
        'Album.published'=>1
      )
    ));
    $this->set(compact('albums'));
  }

  public function add_album(){
    if($this->request->is('post')){
      $this->request->data['Album']['website_id'] = $this->Session->read('Auth.User.Website.id');
      if($this->Album->save($this->request->data)){
        $this->Session->setFlash('Album successfully created','flash_success');
        $this->redirect(array('controller'=>'albums','action'=>'manage_album',$this->Album->id));
      }
      $this->Session->setFlash('Unable to create album','flash_error');
    }
  }

  public function manage_album(){

  }
}
?>
