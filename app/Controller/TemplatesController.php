<?php
App::uses('AppController','Controller');
class TemplatesController extends AppController {
  var $components = array('RequestHandler');

  public function get_templates(){
      $this->Template->recursive = -1;
      $templates = Cache::read('templates', 'short');
      if (empty($templates)) {
          $templates = $this->Template->find('all', array('conditions'=>array('Template.public'=>1),'order' => 'rand()'));
          Cache::write('templates', $templates, 'long');
      }
      return $templates;
  }


}
?>
