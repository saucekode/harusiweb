<?php
class SubscriptionsController extends AppController {
  public function get_subscriptions(){
    $this->Subscription->recursive = -1;
    $subscriptions = $this->Subscription->find('all');
    if(!empty($subscriptions)){
      return $subscriptions;
    }
    return false;
  }

  //To activate a subscription for a website
  public function activate_sub($id,$website_id){
    $this->Subscription->recursive = -1;

    $sub = $this->Subscription->findById($id);
    $this->Session->write('preferred_subscription',$sub);

    if($sub['Subscription']['price']==0){
      $active = 1;
    } else{
      $active = 0;
    }

    $details = array(
      'website_id'=>$website_id,
      'subscription_id'=>$sub['Subscription']['id'],
      'active'=>$active,
      'sms'=>$sub['Subscription']['sms'],
      'donation_percent'=>$sub['Subscription']['donation_percent'],
      'no_of_images'=>$sub['Subscription']['no_of_images'],
      'image_size'=>$sub['Subscription']['image_size'],
      'blog_posts'=>$sub['Subscription']['blog_posts'],
      'no_of_albums'=>$sub['Subscription']['no_of_albums'],
      'price'=>$sub['Subscription']['price'],
    );
    $this->Subscription->Subscriber->save($details);
    if($sub['Subscription']['price']==0){
      return $this->redirect($this->referer());
    } else {
      return $this->redirect('/subscribers/pay_subscription/'.$website_id);
    }
  }
}
?>
