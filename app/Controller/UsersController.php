<?php
App::uses('AppController','Controller');
class UsersController extends AppController {

  public $components = array(
      'Email',
      'Paginator',
      'Session',
      'Auth' => array(
          'authenticate' => array(
              'Form' => array(
                  'passwordHasher' => 'Blowfish'
              )
          )

      ),
  );

  public function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allow('activate','checklogin','login','forgot_password','reset_your_account_password');
  }

  public function checklogin(){
      if($this->Session->check('Auth.User')){
          return true;
      }
      return false;
  }

  public function dashboard(){

  }
  public function login(){

    if($this->checklogin()){
        $this->redirect(array('action' => 'dashboard'));
    }

    if ($this->request->is('post')) {
        $user = $this->User->findByUsername($this->data['User']['username']);

        if (empty($user)) {
            $this->Session->setFlash(__('Account does not exist'),'flash_error');
        } elseif ($user['User']['status'] == 1) {
            if ($this->Auth->login()) {

              $ip = $this->request->clientIp();
              $login_time = date("Y-m-d H:i:s");
              $this->User->id = $user['User']['id'];
              $this->User->saveField('last_login_ip',$ip);
              $this->User->saveField('last_login',$login_time);

              //$this->redirect($this->Auth->redirectUrl());
              $this->redirect('/dashboard');
            } else {
                $this->Session->setFlash(__('Username or password is incorrect.'),'flash_error');
            }
        } else {

            $this->Session->setFlash(__('This account is not active'),'flash_error');
        }

    }

  }

  public function logout() {
      $this->Auth->logout();
      $this->Session->setFlash(__("Thank you for using HarusiWeb. See you soon"),'flash_success');
      $this->Session->delete('Auth');
      $this->redirect('/login');
  }

  public function account_settings(){
    $id = $this->Session->read('Auth.User.id');
    $user = $this->User->findById($id);
    $this->set(compact('user'));

    if ($this->request->is(array('post', 'put'))) {
        $this->User->id = $id;
        if ($this->User->save($this->request->data)) {
            $this->Session->setFlash(__('Your profile has been updated.'),'flash_success');
        } else{
          $this->Session->setFlash(__('Unable to update your profile.'),'flash_error');
        }
    }

    if (!$this->request->data) {
        $this->request->data = $user;
    }
  }


  function forgot_password() {
      if ($this->request->is('post') || $this->request->is('put')) {
          $user = $this->User->findByEmail($this->data['User']['email']);
          if (empty($user)) {
              $this->Session->setFlash(__('Email does not exist'),'flash_error');
          } else {
              //$user = $this->__generatePasswordToken($user);

              $token = "";
              for ($i = 0; $i < 100; $i++) {
                  $d = rand(1, 100000) % 2;
                  $d ? $token .= chr(rand(33,79)) : $token .= chr(rand(80,126));
              }

              (rand(1, 100000) % 2) ? $token = strrev($token) : $token = $token;

              // Generate hash of random string
              $hash = Security::hash($token, 'sha256', true);;
              for ($i = 0; $i < 20; $i++) {
                  $hash = Security::hash($hash, 'sha256', true);
              }

              $this->User->id = $user['User']['id'];

              $this->User->saveField('reset_password_token',$hash);
              $this->User->saveField('token_created_at',date('Y-m-d H:i:s'));

              $this->__sendForgotPasswordEmail($user['User']['id']);
              $this->Session->setFlash(__('Password reset instructions have been sent to your email address.'),'flash_success');
              $this->redirect('/users/login');
          }
      }
  }

  /**
   * Allow user to reset password if $token is valid.
   * @return
   */
  function reset_your_account_password($reset_password_token = null) {
      if (!$reset_password_token) {
              $this->Session->setFlash('Please provide a valid token reset');
              $this->redirect(array('action'=>'login'));
          }

          $user = $this->User->findByResetPasswordToken($reset_password_token);
          if (!$user) {
              $this->Session->setFlash(__('This token does not exist'),'flash_error');
              $this->redirect(array('action'=>'login'));
          }

          if ($this->request->is('post') || $this->request->is('put')) {
              $this->User->id = $user['User']['id'];
              $name = $user['User']['name'];
              if ($this->User->save($this->request->data)) {
                  App::uses('CakeEmail', 'Network/Email');
                  $email = new CakeEmail('default');

                  $email->from(array('helpdesk@quickservice.ng'=>'Quickservice.ng'));
                  $email->to($user['User']['email']);
                  $email->subject('Confirmation of password change');
                  $email->template('password_reset_success', null);
                  $email->viewVars(compact('name','user'));
                  $email->emailFormat('html');
                  $email->send();
                  $this->Session->setFlash(__('Password successfully changed'),'flash_success');
                  $this->redirect(array('action' => 'login'));
              }else{
                  $this->Session->setFlash(__('Unable to update your password.'),'flash_error');
              }
          }

      if (!$this->request->data) {
          $this->request->data = $user;
      }
  }

  public function delete($id) {
      if ($this->request->is('get')) {
          throw new MethodNotAllowedException();
      }
      if ($this->User->delete($id)) {
          $this->Session->setFlash(__('The user with id: %s has been deleted.', h($id)));
      }
  }


  /**
   * Generate a unique hash / token.
   * @param Object User
   * @return Object User
   */
  function __generatePasswordToken($user) {
      if (empty($user)) {
          return null;
      }

      // Generate a random string 100 chars in length.
      $token = "";
      for ($i = 0; $i < 100; $i++) {
          $d = rand(1, 100000) % 2;
          $d ? $token .= chr(rand(33,79)) : $token .= chr(rand(80,126));
      }

      (rand(1, 100000) % 2) ? $token = strrev($token) : $token = $token;

      // Generate hash of random string
      $hash = Security::hash($token, 'sha256', true);;
      for ($i = 0; $i < 20; $i++) {
          $hash = Security::hash($hash, 'sha256', true);
      }

      $user['User']['reset_password_token'] = $hash;
      $user['User']['token_created_at']     = date('Y-m-d H:i:s');

      return $user;
  }

  /**
   * Validate token created at time.
   * @param String $token_created_at
   * @return Boolean
   */
  function __validToken($token_created_at) {
      $expired = strtotime($token_created_at) + 86400;
      $time = strtotime("now");
      if ($time < $expired) {
          return true;
      }
      return false;
  }

  /**
   * Sends password reset email to user's email address.
   * @param $id
   * @return
   */
  function __sendForgotPasswordEmail($id = null) {
      if (!empty($id)) {
          $this->User->id = $id;
          $User = $this->User->read();

          App::uses('CakeEmail', 'Network/Email');
          $email = new CakeEmail('default');

          $email->from(array('helpdesk@quickservice.ng'=>'Quickservice.ng'));
          $email->to($User['User']['email']);
          $email->subject('Password recovery');
          $email->template('reset_password_request', null);
          $email->viewVars(compact('User'));
          $email->emailFormat('html');
          $email->send();

          return true;
      }
      return false;
  }


  function __sendActivationEmail($id = null) {
      if (!empty($id)) {
          $this->User->id = $id;
          $user = $this->User->read();

          App::uses('CakeEmail', 'Network/Email');
          $email = new CakeEmail('default');

          $email->from(array('helpdesk@quickservice.ng'=>'Quickservice.ng'));
          $email->to($user['User']['email']);
          $email->subject('Activate your account');
          $email->template('account_activation', null);
          $email->viewVars(compact('user'));
          $email->emailFormat('html');
          $email->send();

          return true;
      }
      return false;
  }




  function __generateActivationToken(){
      $token = "";
      for ($i = 0; $i < 100; $i++) {
          $d = rand(1, 100000) % 2;
          $d ? $token .= chr(rand(33,79)) : $token .= chr(rand(80,126));
      }

      (rand(1, 100000) % 2) ? $token = strrev($token) : $token = $token;

      // Generate hash of random string
      $hash = Security::hash($token, 'sha256', true);;
      for ($i = 0; $i < 20; $i++) {
          $hash = Security::hash($hash, 'sha256', true);
      }

      return $hash;
  }


  public function activate($activation_token){
      $user = $this->User->findByActivationToken($activation_token);
      if(!empty($user)){
          if($user['User']['status'] == 1){
              $this->Session->setFlash('Your account is already active','flash_error');
              $this->redirect('/login');
          } else {
              $this->User->id = $user['User']['id'];
              $this->User->saveField('status',1);
              $this->Session->setFlash('Congratulations! Your account is now active','flash_success');
              $this->redirect('/login');
          }
      }
      $this->Session->setFlash('Token not active','flash_error');
      $this->redirect('/login');
  }
}
?>
