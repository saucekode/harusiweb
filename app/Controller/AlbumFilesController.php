<?php
class AlbumFilesController extends AppController {

  public function count_website_images($website_id){
    $images = $this->AlbumFile->find('count',array(
      'conditions'=>array(
        'Album.website_id'=>$website_id
      )
    ));
    return $images;
  }
}
?>
