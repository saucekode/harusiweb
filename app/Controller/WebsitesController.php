<?php
App::uses('AppController','Controller');
class WebsitesController extends AppController {
  var $components = array('RequestHandler');

  public function upload_pix($file,$filename,$type){
    $dest_file = WWW_ROOT . "bride_groom/".$type."/" . $filename;

    move_uploaded_file($file['tmp_name'],$dest_file );
  }

  public function home(){
  }

  public function edit_website($id, $unique_code){
    $website = $this->Website->findByIdAndUniqueCode($id, $unique_code);
    $this->set(compact('website'));

    if ($this->request->is(array('post', 'put'))) {

      //Upload new groom pix or unset the array
      if(!empty($this->request->data['Website']['groom_pix']['name'])){
        $groom_filename = $website['Website']['groom_pix'];
        $this->upload_pix($this->request->data['Website']['groom_pix'],$groom_filename,'groom');
        $this->request->data['Website']['groom_pix']=$groom_filename;
      } else{
        unset($this->request->data['Website']['groom_pix']);
      }

      //Upload new bride pix or unset the array
      if(!empty($this->request->data['Website']['bride_pix']['name'])){
        $bride_filename = $website['Website']['bride_pix'];
        $this->upload_pix($this->request->data['Website']['bride_pix'],$bride_filename,'bride');
        $this->request->data['Website']['bride_pix'] = $bride_filename;
      } else{
        unset($this->request->data['Website']['bride_pix']);
      }

      $this->Website->id = $id;
      if ($this->Website->save($this->request->data)) {
        //To update already saved address
        if(!empty($this->request->data['Address'])){
          foreach ($this->request->data['Address'] as $key => $address) {
            if(!empty($address['id'])){
              $this->Website->Address->id = $address['id'];
              $this->Website->Address->save($address);
            }
          }
        }

        //If the address is new
        if(!empty($this->request->data['sfsf'])){
          foreach ($this->request->data['sfsf'] as $key => $address) {
            $new_address = array(
              'name'=>$address['name'],
              'website_id'=>$website['Website']['id'],
              'street'=>$address['street'],
              'city'=>$address['city'],
              'country'=>$address['country'],
              'date'=>$address['date'],
              'time'=>$address['time'],
              'timezone'=>$address['timezone']
            );
            $this->Website->Address->create();
            $this->Website->Address->save($new_address);
          }
        }

        $this->Session->setFlash(__('Your website has been updated.'),'flash_success');
        return $this->redirect('/dashboard');
      } else{
        $this->Session->setFlash(__('Unable to update your website.'),'flash_error');
      }
    }

    if (!$this->request->data) {
        $this->request->data = $website;
    }

  }

  public function get_user_website(){
    $user_id = $this->Session->read('Auth.User.id');
    $website = $this->Website->findByUserId($user_id);
    if(!empty($website)){
      $this->Session->write('current_website',$website);
    }
  }

  public function submit_form_home(){
    $this->layout = false;
    if($this->request->is('post')){
      if(!empty($this->request->data)){
        $pwd = strtolower(substr(md5(uniqid(rand(), true)),0,20));
        $user_details = array(
          'user_group_id'=>$this->Website->User->UserGroup->getGroupId('Client'),
          'name'=>$this->request->data['full_name'],
          'email'=>$this->request->data['email'],
          'phone'=>$this->request->data['phone_no'],
          'password'=>$pwd,
          'token'=>$pwd.strtotime('now')
        );
        $check_email = $this->Website->User->findByEmail($user_details['email']);
        if(empty($check_email)){
          $this->Website->User->save($user_details);
          $this->Session->setFlash('Your account was successfully created','flash_success');
          echo "success";
        } else {
          echo "failed";
        }
      }
    }
  }

  public function add_website(){

  }

  public function submit_add_website(){
    $this->layout = false;
    if($this->request->is('post')){
      if(!empty($this->request->data)){
        $random_code = strtolower(substr(md5(uniqid(rand(), true)),0,20));
        $this->request->data['unique_code']=$random_code;
        $website_details = $this->request->data;
        $check_subdomain = $this->Website->findByShortUrl($this->request->data['short_url']);
        if(empty($check_subdomain)){

      $groom_filename = 'groom'.time().'_'.$_FILES['groom_pix']['name'];
          $this->upload_pix($_FILES['groom_pix'],$groom_filename,'groom');
          $website_details['groom_pix']=$groom_filename;

      $bride_filename = 'bride'.time().'_'.$_FILES['bride_pix']['name'];
          $this->upload_pix($_FILES['bride_pix'],$bride_filename,'bride');
          $website_details['bride_pix'] = $bride_filename;

          $website_details['step'] = 3;

          $this->Website->save($website_details);
          $this->Session->setFlash('First step completed. You have done well. Please fill this last details','flash_success');
          echo "success";
        } else {
          echo "failed";
        }
      }
    }
  }

  public function add_website_step_two($short_url, $unique_code){
    $website = $this->Website->findByShortUrlAndUniqueCode($short_url,$unique_code);
    if(!empty($website)){
      $this->set(compact('website'));
    }
  }

  public function submit_add_website_step_two(){
    $this->layout = false;
    if($this->request->is(array('post', 'put'))){
      if(!empty($this->request->data)){
        $web_details = $this->request->data;
        $short_url = $web_details['short_url'];
        $unique_code = $web_details['unique_code'];
        $get_website = $this->Website->findByShortUrlAndUniqueCode($short_url,$unique_code);

        if(!empty($get_website)){
          $this->Website->id = $get_website['Website']['id'];
          $this->Website->save($web_details);
          foreach ($web_details['Address'] as $key => $address) {
            $address['website_id'] = $get_website['Website']['id'];
            $this->Website->Address->save($address);
          }

          $this->Session->setFlash('Hurray! Your website is now live','flash_success');
          echo "success";
        } else {
          echo "failed";
        }
      }
    }
  }

  public function select_template($short_url, $unique_code){
    $website = $this->Website->findByShortUrlAndUniqueCode($short_url,$unique_code);
    if(!empty($website)){
      $this->set(compact('website'));
    }
  }

  public function submit_select_template(){
    $this->layout = false;
    if($this->request->is(array('post', 'put'))){
      if(!empty($this->request->data)){
        $web_details = $this->request->data;
        $short_url = $web_details['short_url'];
        $unique_code = $web_details['unique_code'];
        $get_website = $this->Website->findByShortUrlAndUniqueCode($short_url,$unique_code);

        if(!empty($get_website)){
          $this->Website->id = $get_website['Website']['id'];
          $this->Website->save($web_details);

          $this->Session->setFlash('Hurray! Your website is now live','flash_success');
          echo "success";
        } else {
          echo "failed";
        }
      }
    }
  }

  public function googlefonts(){
    $this->layout = false;
  }

  public function view_website($short_url){

  }


}
?>
