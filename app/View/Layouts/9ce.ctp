<html lang="en"> <!--<![endif]-->


	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title><?php echo $title_for_layout;?>  | Harusiweb.com</title>
		<meta name="keywords" content="wedding websites, free wedding website, wedding, african wedding">
		<meta name="description" content="No.1 Free Wedding Website | Wedding Reservations | Wedding Promotions">

		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- Libs CSS -->
		<link href="<?php echo $this->webroot."9ce/"?>css/bootstrap.css" rel="stylesheet">
		<link href="<?php echo $this->webroot."9ce/"?>css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo $this->webroot."9ce/"?>css/owl.carousel.css" rel="stylesheet">
		<link href="<?php echo $this->webroot."9ce/"?>css/prettyPhoto.css" rel="stylesheet">
		<link href="<?php echo $this->webroot."9ce/"?>css/flexslider.css" rel="stylesheet">
		<link href="<?php echo $this->webroot."9ce/"?>css/theme-elements.css" rel="stylesheet">
		<link href='<?php echo $this->webroot."websites/googlefonts"?>' type='text/css' rel='stylesheet'/>

		<!-- Template CSS -->
		<link href="<?php echo $this->webroot."9ce/"?>css/style.css" rel="stylesheet">

		<!-- Responsive CSS -->
		<link href="<?php echo $this->webroot."9ce/"?>css/responsive.css" rel="stylesheet">
		<link href="<?php echo $this->webroot."9ce/"?>css/button.css" rel="stylesheet">

		<!--For DateTimePicker-->
		<link rel="stylesheet" href="<?php echo $this->webroot?>js/bootstrap-timepicker/css/bootstrap-timepicker.css" />

		<!-- Favicons -->
		<!--link rel="shortcut icon" href="img/icons/favicon.ico"-->

		<!-- Google Fonts -->
		<!--link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'-->

	</head>


	<body>
    <?php echo $this->element('9ce_header');?>



		<!-- PAGE CONTENT WRAPPER
		============================================= -->
		<div id="content_wrapper">



			<?php echo $this->fetch('content');?>



			<?php echo $this->element('9ce_footer');?>



		</div> <!-- PAGE CONTENT WRAPPER -->



		<!-- EXTERNAL SCRIPTS
		============================================= -->
		<script src="<?php echo $this->webroot."9ce/"?>js/jquery-2.1.4.min.js" type="text/javascript"></script>
		<script src="<?php echo $this->webroot."9ce/"?>js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo $this->webroot."9ce/"?>js/modernizr.custom.js" type="text/javascript"></script>
		<script src="<?php echo $this->webroot."9ce/"?>js/jquery.easing.js" type="text/javascript"></script>
		<script defer src="<?php echo $this->webroot."9ce/"?>js/jquery.scrollto.js"></script>
		<script src="<?php echo $this->webroot."9ce/"?>js/owl.carousel.js" type="text/javascript"></script>
		<script defer src="<?php echo $this->webroot."9ce/"?>js/count-to.js"></script>
		<script defer src="<?php echo $this->webroot."9ce/"?>js/jquery.appear.js"></script>
		<script src="<?php echo $this->webroot."9ce/"?>js/jquery.prettyPhoto.js" type="text/javascript"></script>
		<script src="<?php echo $this->webroot."9ce/"?>js/jquery.mixitup.js" type="text/javascript"></script>
		<script src="<?php echo $this->webroot."9ce/"?>js/retina.js" type="text/javascript"></script>
		<script defer src="<?php echo $this->webroot."9ce/"?>js/jquery.flexslider.js" type="text/javascript"></script>
		<script defer src="<?php echo $this->webroot."9ce/"?>js/jquery.validate.min.js" type="text/javascript"></script>


		<link rel="stylesheet" href="<?php echo $this->webroot."date_picker/"?>jquery-ui.css">
	<script src="<?php echo $this->webroot."date_picker/"?>jquery-1.10.2.js"></script>
	<script src="<?php echo $this->webroot."date_picker/"?>jquery-ui.js"></script>
	<script>
	$(function() {
		$( ".cloned-input" ).datepicker(
			{
				minDate: -0,
				dateFormat: 'd MM, yy'
			}
		).attr('readonly','readonly');
	});
	</script>

	<!--For the picture for bride and groom-->
	<!--script src="<?php echo $this->webroot;?>js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="<?php echo $this->webroot;?>js/bootstrap.min.js" type="text/javascript"></script>
	<!--   plugins 	 -->
	<!--script src="<?php echo $this->webroot;?>js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
		<!--  methods for manipulating the wizard and the validation -->
	<!--End For the picture for bride and groom-->
<script src="<?php echo $this->webroot?>js/bootstrap-timepicker/bootstrap-timepicker.js"></script>
<script type="text/javascript">
	 jQuery('.time-picker-new').timepicker({
			 minuteStep: 5,
			 defaultTime: 'current',
			 showInputs: false,
			 disableFocus: true
	 });
</script>

<?php echo $this->element('sql_dump'); ?>

  </body>
  </html>
