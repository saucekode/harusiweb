<?php $this->set('title_for_layout','My Albums');?>

<style type="text/css">
@import url(<?php echo $this->webroot."9ce/css/fancy-lines.css"?>);
</style>

<section style="padding-top:30px;padding-bottom:20px;">
	<div class="container">

    <!-- SECTION TITLE -->
		<div class="row">
			<div class="col-sm-12 titlebar" style="margin-bottom:0px">
        <?php echo $this->Session->flash();?>

				<h3>My Albums</h3><p></p>

			</div>
		</div>	<!-- END SECTION TITLE -->

    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="featured-boxes">
          <div class="featured-box featured-box-primary align-left" style="margin-top:0px;">
            <div class="box-content">
              <?php echo $this->element('user_leftbar');?>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-9 col-sm-8 col-xs-12">
        <?php echo $this->Session->flash();?>

        <div class="row">
          <div class="col-sm-12">
            <div class="featured-box featured-box-primary align-left mt-xlg" style="margin-top:0px;background:#F5F5F5">
              <div class="box-content">

<?php
$check_subscription = $this->requestAction('/subscribers/check_active_subscription/'.$website_id);
?>

                <?php if(!$check_subscription):?>



                <div class="alert alert-danger alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong>Oops! You do not have an active subscription.</strong>
                  <br>
                  <i class="fa fa-arrow-circle-o-down"></i>Please choose any of our flexible plans below to enjoy this feature.
								</div>

                <?php echo $this->element('pricing_plans');?>



							<?php else:?>


								<?php echo $this->element('subscriber_details');?>

								<hr>

								<a href="<?php echo $this->webroot."albums/add_album"?>" class="btn btn-primary btn-sm">Add a new album</a><br><br>

								<div class="table-responsive">
										<table class="table table-striped mb-none">
											<thead>
												<tr>
													<th width="10%">#</th>
													<th width="45%">Album Title</th>
													<th width="20%">No. of Images</th>
													<th width="25%">Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php if(!empty($albums)):?>

													<?php $i = 0;?>
													<?php foreach ($albums as $key => $album):?>
														<?php $i++;?>
													<tr>
														<td><?php echo $i;?></td>
														<td><?php echo $album['Album']['title'];?></td>
														<td>
															<u>
																<a href="#"><?php echo $album['Album']['how_many_images'];?></a>
															</u>
														</td>
														<td>
															<a href="#" class="btn btn-blue btn-xs"><i class="fa fa-edit white"></i> Edit </a>
															<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-times white"></i> Delete </a>
														</td>
													</tr>
												<?php endforeach;?>

											<?php else:?>

												<tr>
													<td colspan="4" class="text-center">No album found</td>
												</tr>

											<?php endif;?>
											</tbody>
										</table>
									</div>

              <?php endif;?>

              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
