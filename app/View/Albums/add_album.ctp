<?php $this->set('title_for_layout','Add a New Album');?>

<section style="padding-top:30px;padding-bottom:20px;">
	<div class="container">

    <!-- SECTION TITLE -->
		<div class="row">
			<div class="col-sm-12 titlebar" style="margin-bottom:0px">
        <?php echo $this->Session->flash();?>

				<h3>ADD A NEW ALBUM</h3><p></p>

			</div>
		</div>	<!-- END SECTION TITLE -->

    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="featured-boxes">
          <div class="featured-box featured-box-primary align-left" style="margin-top:0px;">
            <div class="box-content">
              <?php echo $this->element('user_leftbar');?>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-9 col-sm-8 col-xs-12">
        <?php echo $this->Session->flash();?>

        <div class="row">
          <div class="col-sm-10 col-md-onset-2">
            <div class="featured-box featured-box-primary align-left mt-xlg" style="margin-top:0px;background:#F5F5F5">
              <div class="box-content">
                <?php echo $this->Form->create('Album', array('class'=>'form-horizontal','inputDefaults' => array('label' => false, 'div' => false))); ?>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Title</label>
												<?php echo $this->Form->input('title',array('class'=>'form-control input-md'))?>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Description</label>
												<?php echo $this->Form->textarea('description',array('class'=>'form-control input-md','rows'=>3))?>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-4 col-md-offset-4 col-md-onset-4 text-center">
                        <input type="submit" value="Submit" class="btn btn-primary mb-xl" data-loading-text="Loading...">
                      </div>
                    </div>
                  </div>


                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>



<?php echo $this->element('validationscript');?>
<script>
$(function(){

  jQuery('#AlbumAddAlbumForm').validate({
    rules: {
        "data[Album][title]": {
            required:true
        },
        "data[Album][description]":{
          required:true
        }
    }
  });

});
</script>
