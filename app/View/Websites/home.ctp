<?php $this->set('title_for_layout','No.1 Free Wedding Website | Wedding Reservations | Wedding Promotions');?>

<!-- INTRO
============================================= -->
<section id="intro">
  <div class="container">
    <div id="intro_content" class="row">


      <!-- INTRO TEXT -->
      <div class="col-md-7 intro_text">

        <h2>Create Your <span>Free Wedding Website</span></h2>
        <p><strong>Multiple templates + Unlimited Guests Reservation + Unlimited Photo Albums</strong></p>

        <p>Need a platform for guests reservation, directions to venue, SMS updates to guests, archive photo albums? You also dream of that wedding that's got everyone talking on social media? Look no further, <strong>harusiweb.com</strong> is the perfect place!
        </p>

        <!-- INTRO BUTTONS -->
        <div class="intro_button">
          <a class="btn btn-tra" href="#">Learn more</a>
          <a class="btn" href="#">Get started now</a>
        </div>

      </div>	<!-- END TEXT -->


      <!-- INTRO REGISTER FORM -->
      <div id="intro_form" class="col-md-5 form_register text-center">

        <!-- Register Form -->
        <script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js";?>"></script>

        <form id="signup_form" method="post" action="<?php echo $this->webroot."websites/submit_form_home";?>" class="row">

          <h4>Create your free website now!</h4>

          <p class="result_msg">
          </p>
          <div class="col-md-12">
            <input class="form-control" type="text" name="full_name" placeholder="Enter your full name">
          </div>

          <div class="col-md-12">
            <input class="form-control" type="text" name="email" placeholder="Enter your email">
          </div>

          <div class="col-md-12">
            <input class="form-control" type="text" name="phone_no" placeholder="Phone Number">
          </div>

          <p>By signing up, you agree to our <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a></p>

          <!-- Submit Button -->
          <div id="form_register_btn" class="text-center">
            <input class="btn" type="submit" value="Register Now" >
          </div>

        </form>	<!-- End Register Form -->

      </div>    <!-- END INTRO REGISTER FORM -->


    </div>	 <!-- End Intro Content -->
  </div>    <!-- End container -->
</section>	<!-- END INTRO -->

<?php echo $this->element('validationscript');?>
<script>
$(function(){
  $('#signup_form').validate({
    rules: {
        "full_name": {
            required:true,
        },
        "email": {
            required:true,
            email:true
        },
        "phone_no":{
          required:true,
          digits:true,
          minlength:6
        }
    },
    messages:{
      "full_name":"Please enter your full name",
      "email":{
        "required":"Please enter your email address",
        "email":"Please enter a valid email address"
      },
      "phone_no":{
        "required":"Your phone number is required",
        "digits":"Only digits are allowed"
      }
    },
    submitHandler: function(form) {
          var link_url = '<?php echo $this->webroot."websites/submit_form_home";?>';
          $.ajax({
              type: "POST",
              url: link_url,
              data: $('#signup_form').serialize(),
              dataType: "html",
              cache: false,
              success: function(response){
                if(response == 'success'){
                  $(".result_msg").html("<span style='color:green'>Your registration was successful");
                  var link_redirect = '<?php echo $this->webroot."websites/add_website"?>';
                  window.location.href=link_redirect;
                } else {
                  $(".result_msg").html("<span style='color:red'>User already exists");
                }
              }
          });
          return false;
    }
  });
});
</script>
