<?php $this->set('title_for_layout','Create your website - Step 3');?>

<section style="padding-top:40px;padding-bottom:20px;">
	<div class="container">
<style type="text/css">
@import url(<?php echo $this->webroot."9ce/css/fancy-lines.css"?>);
body {
  background: url(<?php echo $this->webroot."9ce/img/blurry-blue.jpg"?>);
  background-size: cover;
}
.cc-selector input{
    margin:0;padding:0;
    -webkit-appearance:none;
       -moz-appearance:none;
            appearance:none;
}

.cc-selector input:active +.drinkcard-cc{opacity: .9;}
.cc-selector input:checked +.drinkcard-cc{
    -webkit-filter: none;
       -moz-filter: none;
            filter: none;
}
.drinkcard-cc{
    cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    -webkit-transition: all 100ms ease-in;
       -moz-transition: all 100ms ease-in;
            transition: all 100ms ease-in;
    -webkit-filter: brightness(1.8) grayscale(1) opacity(.7);
       -moz-filter: brightness(1.8) grayscale(1) opacity(.7);
            filter: brightness(1.8) grayscale(1) opacity(.7);
}
.drinkcard-cc:hover{
    -webkit-filter: brightness(1.2) grayscale(.5) opacity(.9);
       -moz-filter: brightness(1.2) grayscale(.5) opacity(.9);
            filter: brightness(1.2) grayscale(.5) opacity(.9);
}
</style>

		<!-- SECTION TITLE -->
		<div class="row">
			<div class="col-sm-12 titlebar" style="margin-bottom:0px">
        <?php echo $this->Session->flash();?>

				<h3 style="color:#fff">Create Your Free Wedding Website - Final Step</h3>
				<p style="color:#fff">Please provide the following details to set up your website</p>

			</div>
		</div>	<!-- END SECTION TITLE -->

    <div class="row">
      <div class="col-md-12">
        <div class="featured-boxes">
          <div class="row">
<div class="col-sm-8 col-md-offset-2 col-md-onset-2">
<div class="featured-box featured-box-primary align-left mt-xlg" style="margin-top:15px;background:#F5F5F5">
  <div class="box-content">
		<p class="result_msg"></p>

		<script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js";?>"></script>
    <?php echo $this->Form->create('Website', array('action'=>'submit_add_website','type'=>'file','class'=>'form-horizontal', 'inputDefaults' => array(
        'label' => false,
        'div'=>false)));?>

      <p class="subtitle fancy"><span>Choose your preferred Website Template<br><br><br><small class="text-danger">To choose a template, click on any of these images.</small></span></p><br>

      <div class="row team_members_wrapper cc-selector">

				<?php $templates = $this->requestAction('/templates/get_templates');?>
				<?php foreach($templates as $template):?>
						<!-- TEAM MEMBER #1 -->
						<div class="col-xs-6 col-sm-6 col-md-3">
							<div class="team-member">

								<!-- Team Member Photo  -->
								<div class="img-block">
									<input id="<?php echo $template['Template']['short_title'];?>" type="radio" name="data[Website][template_id]" value="<?php echo $template['Template']['id'];?>" checked="true"/>

									<label class="drinkcard-cc img-responsive" for="<?php echo $template['Template']['short_title'];?>">
										<img class="img-responsive" src="<?php echo $this->webroot?>img/thumbs/team-1.jpg">
									</label>
								</div>

								<!-- Team Member Meta  -->
								<div class="team-img-meta text-center">
									<a class="btn btn-xs btn-blue" href="#"><i class="fa fa-search-plus"></i> Preview</a>
								</div>

							</div>
						</div>	<!-- END TEAM MEMBER #1 -->
					<?php endforeach;?>


					</div>


        <div class="form-group">
          <div class="col-md-8">
            <span class="remember-box checkbox" style="margin-top: 20px;">
              <label style="padding-left: 0px">
                Already have a website? <a href="/signup">Promote it Now.</a>
              </label>
            </span>
          </div>
          <div class="col-md-4">
            <input type="submit" value="Submit" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
          </div>
        </div>

    </form>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>

	</div>	    <!-- End container -->
</section>


<?php echo $this->element('validationscript');?>
<script>
$(function(){

	jQuery.validator.addMethod("lettersonly", function(value, element) {
		return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Letters only please");

	jQuery.validator.addMethod("noSpace", function(value, element) {
		return value.indexOf(" ") < 0 && value != "";
	}, "Empty space is not allowed");

	jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
	}, "Only letters and numbers are allowed");

  $('#WebsiteSubmitAddWebsiteForm').validate({
    rules: {
        "data[Website][template_id]": {
            required:true
        }
    },
    messages:{
      "data[Website][template_id]":{
        "required":"Please select a template"
      }
    },
    submitHandler: function(form) {
			var input_shorturl_val = '<?php echo $website['Website']['short_url'];?>';
			var input_unique_code_val = '<?php echo $website['Website']['unique_code'];?>';

			var input_shorturl = $("<input>")
			        .attr("type", "hidden")
			        .attr("name", "short_url").val(input_shorturl_val);
			$('#WebsiteSubmitAddWebsiteForm').append($(input_shorturl));

			var input_unique_code = $("<input>")
			       .attr("type", "hidden")
			       .attr("name", "unique_code").val(input_unique_code_val);
			       $('#WebsiteSubmitAddWebsiteForm').append($(input_unique_code));

			var formURL = '<?php echo $this->webroot."websites/submit_select_template"?>';
			var formData = new FormData(form);
			$.ajax({
			  url: formURL,
				type: "POST",
				data: formData,
				contentType: false,
				cache: false,
				processData: false,
				success: function(data) {
					if(data == 'success'){
						var link_redirect = '<?php echo $this->webroot.$website['Website']['short_url']?>';
						window.location.href=link_redirect;
					} else {
						alert("Please select a template");
						$("#short_url").focus();
					}
				}
			});
			return false;

    }
  });
});
</script>
