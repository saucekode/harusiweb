<?php $this->set('title_for_layout','Create your website - Step 3');?>
<section style="padding-top:40px;padding-bottom:20px;">
	<div class="container">
<style type="text/css">
@import url(<?php echo $this->webroot."9ce/css/fancy-lines.css"?>);
body {
  background: url(<?php echo $this->webroot."9ce/img/blurry-blue.jpg"?>);
  background-size: cover;
}
.cc-selector input{
    margin:0;padding:0;
    -webkit-appearance:none;
       -moz-appearance:none;
            appearance:none;
}
.visa{background-image:url(http://i.imgur.com/lXzJ1eB.png);}
.mastercard{background-image:url(http://i.imgur.com/SJbRQF7.png);}

.cc-selector input:active +.drinkcard-cc{opacity: .9;}
.cc-selector input:checked +.drinkcard-cc{
    -webkit-filter: none;
       -moz-filter: none;
            filter: none;
}
.drinkcard-cc{
    cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    width:100px;height:70px;
    -webkit-transition: all 100ms ease-in;
       -moz-transition: all 100ms ease-in;
            transition: all 100ms ease-in;
    -webkit-filter: brightness(1.8) grayscale(1) opacity(.7);
       -moz-filter: brightness(1.8) grayscale(1) opacity(.7);
            filter: brightness(1.8) grayscale(1) opacity(.7);
}
.drinkcard-cc:hover{
    -webkit-filter: brightness(1.2) grayscale(.5) opacity(.9);
       -moz-filter: brightness(1.2) grayscale(.5) opacity(.9);
            filter: brightness(1.2) grayscale(.5) opacity(.9);
}
</style>

		<!-- SECTION TITLE -->
		<div class="row">
			<div class="col-sm-12 titlebar" style="margin-bottom:0px">
        <?php echo $this->Session->flash();?>

				<h3 style="color:#fff">Create Your Free Wedding Website - Step 3</h3>
				<p style="color:#fff">Please provide the following details to set up your website</p>

			</div>
		</div>	<!-- END SECTION TITLE -->

    <div class="row">
      <div class="col-md-12">
        <div class="featured-boxes">
          <div class="row">
<div class="col-sm-8 col-md-offset-2 col-md-onset-2">
<div class="featured-box featured-box-primary align-left mt-xlg" style="margin-top:15px;background:#F5F5F5">
  <div class="box-content">
		<p class="result_msg"></p>

		<script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js";?>"></script>
    <?php echo $this->Form->create('Website', array('action'=>'submit_add_website','type'=>'file','class'=>'form-horizontal', 'inputDefaults' => array(
        'label' => false,
        'div'=>false)));?>

      <div class="multi-field-wrapper">
        <p class="subtitle fancy"><span>Add Venue Details, Date and Time</span></p>
        <div class="multi-fields">

          <script type="text/javascript" src="<?php echo $this->webroot."js/underscore-min.js" ;?>"></script>
          <script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js" ;?>"></script>

          <p><br>
              <script id="grade-template" type="text/x-underscore-template">
                  <?php echo $this->element('address');?>
              </script>
            <table id="grade-table" class="table table-striped table-vcenter">
              <tbody>
                  <?php if (!empty($this->request->data['Address'])) :?>
                      <?php for ($key = 0; $key < count($this->request->data['Address']); $key++) :?>
                          <?php echo $this->element('address', array('key' => $key));?>
                      <?php endfor;?>
                  <?php endif;?>
              </tbody>
              <tfoot>
                  <tr>
                      <td class="actions">
                        <a style="margin-left: -7px;" class="add btn btn-xs btn-blue" href="#">
                            <span><i class="fa fa-plus"></i> Add another address</span>
                        </a>
                      </td>
                  </tr>
              </tfoot>
          </table>
          </p>


        </div>
      </div>

      <br>
      <p class="subtitle fancy"><span>RSVP (Reservations/Invites), Donations, SMS</span></p>

      <script type="text/javascript">
          $(function () {
            $("#WebsiteAllowRsvp").click(function () {
            if ($("#WebsiteAllowRsvp").is(":checked")) {
                $("#WebsiteNoOfInvites").show();
                $("#WebsiteNoOfInvites").addClass("required");
                $("#WebsiteNoOfInvites").removeAttr("readonly");
            } else {
                $("#WebsiteNoOfInvites").hide();
                $("#WebsiteNoOfInvites").removeClass("required");
                $("#WebsiteNoOfInvites").attr("readonly","true");
                $("#WebsiteNoOfInvites").val("0");
            }
            });
          });
      </script>


      <div class="form-group">
					<div class="col-md-12">
						<div class="checkbox">
							<label>
                <?php echo $this->Form->checkbox('allow_rsvp', array('hiddenField' => true,'checked'=>'checked'));?>
								I want guests to request for invite on my website.
							</label>
						</div>
            <div class="row">
              <div class="col-sm-6">
                <?php echo $this->Form->input('no_of_invites', array('class'=>'form-control','placeholder'=>'How many? Leave blank for unlimited invites','type'=>'text'));?>
              </div>
              <div class="col-sm-3">
              </div>
            </div>

						<div class="checkbox">
							<label>
								<?php echo $this->Form->checkbox('send_sms', array('hiddenField' => true,'checked'=>'checked'));?>
								I want to send SMS reminder to every invitee. <strong><small>(SMS rates apply. <a href="http://google.com" target="_blank">Click to view pricing</a>)</small></strong>
							</label>
						</div>

            <div class="checkbox">
							<label>
								<?php echo $this->Form->checkbox('accept_donation', array('hiddenField' => true,'checked'=>'checked'));?>
								I want to accept donations on my website. <strong><small>(<a href="http://google.com" target="_blank">View donation terms and conditions</a>)</small></strong>
							</label>
						</div>

					</div>
				</div>


        <div class="form-group">
          <div class="col-md-8">
            <span class="remember-box checkbox" style="margin-top: 20px;">
              <label style="padding-left: 0px">
                Already have a website? <a href="/signup">Promote it Now.</a>
              </label>
            </span>
          </div>
          <div class="col-md-4">
            <input type="submit" value="Submit" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
          </div>
        </div>


    </form>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>

	</div>	    <!-- End container -->
</section>


<script>
$(document).ready(function() {
    var
        gradeTable = $('#grade-table'),
        gradeBody = gradeTable.find('tbody'),
        gradeTemplate = _.template($('#grade-template').remove().text()),
        numberRows = gradeTable.find('.multi-field').length;

    gradeTable
        .on('click', 'a.add', function(e) {
            e.preventDefault();

            if ($('.multi-field', gradeBody).length < 4)

            $(gradeTemplate({key: numberRows++}))
                .hide()
                .appendTo(gradeBody)
                .fadeIn('fast');

            //TO REMOVE THE ID AND OTHERS FROM DATEPICKER
            $('.multi-field').find('.cloned-input').datepicker("destroy").removeAttr("id").datepicker();

            $input = $('.cloned-input');
            // remove still present related DOM objects
            $input.siblings('.ui-datepicker-trigger,.ui-datepicker-apply').remove();
            // remove datepicker object and detach events
            $input.removeClass('hasDatepicker')
            .removeData('datepicker')
            .unbind()
            .datepicker({
              minDate: -0,
              dateFormat: 'd MM, yy'
            }).attr('readonly','readonly');
            //END DATEPICKER

        })
        .on('click', 'a.remove', function(e) {
            e.preventDefault();

            if ($('.multi-field', gradeBody).length > 1)
              $(this)
                .closest('.multi-field')
                .fadeOut('fast', function() {
                  $(this).remove();
                });
        });

        if (numberRows === 0) {
            gradeTable.find('a.add').click();
        }
});
</script>


<?php echo $this->element('validationscript');?>
<script>
$(function(){

	jQuery.validator.addMethod("lettersonly", function(value, element) {
		return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Letters only please");

	jQuery.validator.addMethod("noSpace", function(value, element) {
		return value.indexOf(" ") < 0 && value != "";
	}, "Empty space is not allowed");

	jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
	}, "Only letters and numbers are allowed");

  $('#WebsiteSubmitAddWebsiteForm').validate({
    rules: {
        "data[Website][no_of_invites]": {
            number:true,
            min:0
        }
    },
    messages:{
      "data[Website][no_of_invites]":{
        "number":"Only numbers are allowed"
      }
    },
    submitHandler: function(form) {
      var input_shorturl_val = '<?php echo $website['Website']['short_url'];?>';
     var input_unique_code_val = '<?php echo $website['Website']['unique_code'];?>';

     var input_shorturl = $("<input>")
              .attr("type", "hidden")
              .attr("name", "short_url").val(input_shorturl_val);
     $('#WebsiteSubmitAddWebsiteForm').append($(input_shorturl));

     var input_unique_code = $("<input>")
             .attr("type", "hidden")
             .attr("name", "unique_code").val(input_unique_code_val);
             $('#WebsiteSubmitAddWebsiteForm').append($(input_unique_code));
      var formURL = '<?php echo $this->webroot."websites/submit_add_website_step_two"?>';
      var formData = new FormData(form);
      $.ajax({
        url: formURL,
				type: "POST",
				data: formData,
				contentType: false,
				cache: false,
				processData: false,
				success: function(data) {
					if(data == 'success'){
						var link_redirect = '<?php echo $this->webroot."websites/add_website_step_two"?>';
						window.location.href=link_redirect;
					} else {
						alert("This short url already exists")
						$("#short_url").focus();
					}
				}
      });
      return false;
    }
  });
});
</script>
