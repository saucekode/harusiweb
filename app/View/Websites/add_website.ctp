<?php $this->set('title_for_layout','Create Your Free Wedding Website - Step 2');?>
<section style="padding-top:40px;padding-bottom:20px;">
	<div class="container">
<style type="text/css">
@import url(<?php echo $this->webroot."9ce/css/fancy-lines.css"?>);
body {
  background: url(<?php echo $this->webroot."9ce/img/blurry-blue.jpg"?>);
  background-size: cover;
}
</style>

		<!-- SECTION TITLE -->
		<div class="row">
			<div class="col-sm-12 titlebar" style="margin-bottom:0px">
        <?php echo $this->Session->flash();?>

				<h3 style="color:#fff">Create Your Free Wedding Website - Step 2</h3>
				<p style="color:#fff">Please provide the following details to set up your website</p>

			</div>
		</div>	<!-- END SECTION TITLE -->

    <div class="row">
      <div class="col-md-12">
        <div class="featured-boxes">
          <div class="row">
            <div class="col-sm-6 col-md-offset-3 col-md-onset-3">
              <div class="featured-box featured-box-primary align-left mt-xlg" style="margin-top:15px;background:#F5F5F5">
                <div class="box-content">
									<p class="result_msg"></p>

									<script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js";?>"></script>
                  <form action="<?php echo $this->webroot."websites/submit_add_website";?>" method="post" class="form-horizontal" id="new_website_form" enctype="multipart/form-data">
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Your preferred Website address</label>
													<div class="input-group">
										         <input name="short_url" id="short_url" type="text" class="form-control input-md" placeholder="Examples: YinkaZino, UgoAmaka08, AminuAndAisha">
														 <span for="short_url" class="input-group-addon">.harusiweb.com</span>
										      </div>
													<label for="short_url" generated="true" class="error"></label>
                        </div>
                      </div>
                    </div>

										<p class="subtitle fancy"><span>Bride's Details</span></p>
<!--BEGIN BRIDES DETAILS-->
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Bride's Full Name</label>
													<div class="row">
														<div class="col-sm-6">
															<input name="bride_last_name" class="form-control input-md" type="text" placeholder="Bride's Last Name">
														</div>
														<div class="col-sm-6">
															<input name="bride_first_name" class="form-control input-md" type="text" placeholder="Bride's First Name">
														</div>
													</div>
                        </div>
                      </div>
                    </div>

										<div class="row">
                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Bride's Face Picture</label>
														<input id="bride_pix" name="bride_pix" class="form-control input-md" type="file">
                        </div>
                      </div>
                    </div>

										<div class="row">
                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Brief description about the Bride</label>
														<textarea name="bride_description" class="form-control input-md" type="text" placeholder="Describe the bride in less than 100 words"></textarea>
                        </div>
                      </div>
                    </div>
<!--END BRIDES DETAILS-->

										<p class="subtitle fancy"><span>Groom's Details</span></p>
<!--BEGIN GROOMS DETAILS-->
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Groom's Full Name</label>
													<div class="row">
														<div class="col-sm-6">
															<input name="groom_last_name" class="form-control input-md" type="text" placeholder="Groom's Last Name">
														</div>
														<div class="col-sm-6">
															<input name="groom_first_name" class="form-control input-md" type="text" placeholder="Groom's First Name">
														</div>
													</div>
                        </div>
                      </div>
                    </div>

										<div class="row">
                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Groom's Face Picture</label>
														<input id="groom_pix" name="groom_pix" class="form-control input-md" type="file">
                        </div>
                      </div>
                    </div>

										<div class="row">
                      <div class="form-group">
                        <div class="col-md-12">
                          <label>Brief description about the Groom</label>
														<textarea name="groom_description" class="form-control input-md" type="text" placeholder="Describe the groom in less than 100 words"></textarea>
                        </div>
                      </div>
                    </div>
<!--END GROOMS DETAILS-->


                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-8">
                          <span class="remember-box checkbox" style="margin-top: 20px;">
                            <label style="padding-left: 0px">
                              Already have a website? <a href="/signup">Promote it Now.</a>
                            </label>
                          </span>
                        </div>
                        <div class="col-md-4">
                          <input type="submit" value="Submit" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                        </div>
                      </div>
                    </div>


                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

	</div>	    <!-- End container -->
</section>


<?php echo $this->element('validationscript');?>
<script>
$(function(){

	jQuery.validator.addMethod("lettersonly", function(value, element) {
		return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Letters only please");

	jQuery.validator.addMethod("noSpace", function(value, element) {
		return value.indexOf(" ") < 0 && value != "";
	}, "Empty space is not allowed");

	jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
	}, "Only letters and numbers are allowed");

  $('#new_website_form').validate({
    rules: {
        "short_url": {
            required:true,
						maxlength:15,
						noSpace: true,
						alphanumeric: true
        },
        "groom_first_name": {
            required:true,
						lettersonly:true,
						noSpace: true
        },
        "groom_last_name": {
            required:true,
						lettersonly:true,
						noSpace: true
        },
        "groom_pix": {
            required:true
        },
        "groom_description": {
            maxlength:200
        },
        "bride_first_name": {
            required:true,
						lettersonly:true,
						noSpace: true
        },
        "bride_last_name": {
            required:true,
						lettersonly:true,
						noSpace: true
        },
        "bride_pix": {
            required:true
        },
        "bride_description": {
            maxlength:200
        }
    },
    messages:{
      "short_url":{
				"required":"Short URL field is required",
				"maxlength":"Short URL cannot exceed 15 characters"
			},
      "groom_first_name":{
				"required":"Please enter groom's first name",
				"lettersonly":"Only alphabets are allowed"
			},
      "groom_last_name":{
				"required":"Please enter groom's last name",
				"lettersonly":"Only alphabets are allowed"
			},
      "groom_pix":{
				"required":"Please upload groom's picture"
			},
      "bride_first_name":{
				"required":"Please enter bride's first name",
				"lettersonly":"Only alphabets are allowed"
			},
      "bride_last_name":{
				"required":"Please enter bride's last name",
				"lettersonly":"Only alphabets are allowed"
			},
      "bride_pix":{
				"required":"Please upload bride's picture"
			}
    },
    submitHandler: function(form) {
			var formURL = '<?php echo $this->webroot."websites/submit_add_website"?>';
						var formData = new FormData(form);
						$.ajax({
								url: formURL,
								type: "POST",
								data: formData,
								contentType: false,
								cache: false,
								processData: false,
								success: function(data) {
									if(data == 'success'){
										var link_redirect = '<?php echo $this->webroot."websites/add_website_step_two"?>';
										window.location.href=link_redirect;
									} else {
										alert("This short url already exists")
										$("#short_url").focus();
									}
								}
						});
					return false;
    }
  });
});
</script>
