<?php $this->set('title_for_layout','Edit your website');?>

<style type="text/css">
@import url(<?php echo $this->webroot."9ce/css/fancy-lines.css"?>);
.wizard-card .picture-container{
    position: relative;
    cursor: pointer;
    text-align: center;
}
.wizard-card .picture{
    width: 106px;
    height: 106px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
    border-radius: 50%;
    margin: 5px auto;
    overflow: hidden;
    transition: all 0.2s;
    -webkit-transition: all 0.2s;
}
.wizard-card .picture:hover{
    border-color: #2ca8ff;
}
.wizard-card.ct-wizard-azzure .picture:hover{
    border-color: #2CA8FF;
}
.wizard-card.ct-wizard-green .picture:hover{
    border-color: #05ae0e;
}
.wizard-card.ct-wizard-blue .picture:hover{
    border-color: #3472f7;
}
.wizard-card.ct-wizard-orange .picture:hover{
    border-color: #ff9500;
}
.wizard-card.ct-wizard-red .picture:hover{
    border-color: #ff3b30;
}
.wizard-card .picture input[type="file"] {
    cursor: pointer;
    display: block;
    height: 100%;
    left: 0;
    opacity: 0 !important;
    position: absolute;
    top: 0;
    width: 100%;
}

.wizard-card .picture-src{
    width: 100%;

}
</style>



<section style="padding-top:30px;padding-bottom:20px;">
	<div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="featured-boxes">
          <div class="featured-box featured-box-primary align-left" style="margin-top:0px;">
            <div class="box-content">
              <?php echo $this->element('user_leftbar');?>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-9 col-sm-8 col-xs-12">
        <?php echo $this->Session->flash();?>

        <div class="row">
          <div class="col-sm-10 col-md-onset-2">
            <div class="featured-box featured-box-primary align-left mt-xlg" style="margin-top:0px;background:#F5F5F5">
              <div class="box-content">

                <?php echo $this->Form->create('Website', array('type'=>'file','class'=>'form-horizontal','inputDefaults' => array('label' => false, 'div' => false))); ?>

                  <p class="subtitle fancy"><span>Bride's Details</span></p><br>

									<div class="row wizard-container">
											<div class="col-sm-4 col-sm-onset-1 wizard-card">
												 <div class="picture-container">
															<div class="picture">
																<?php
																if(empty($website['Website']['bride_pix'])){
																	$bride_pix = $this->webroot."img/default_bride.jpg";
																} else {
																	$bride_pix = $this->webroot."bride_groom/bride/".$website['Website']['bride_pix'];
																}
																?>
																	<img src="<?php echo $bride_pix;?>" class="picture-src" id="wizardPicturePreviewBride" title=""/>
																	<?php echo $this->Form->input('bride_pix',array('type'=>'file',"accept"=>"image/*"))?>
															</div>
															<h6>Choose Bride's Picture</h6>
													</div>
											</div>
											<div class="col-sm-7">
													<div class="form-group">
														<label>First Name <small class="error-message">(required)</small></label>
														<?php echo $this->Form->input('bride_first_name',array('class'=>'form-control input-md'))?>
													</div>
													<div class="form-group">
														<label>Last Name <small class="error-message">(required)</small></label>
														<?php echo $this->Form->input('bride_last_name',array('class'=>'form-control input-md'))?>
													</div>
											</div>
									</div>
									<br>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Describe the Bride</label>
												<?php echo $this->Form->textarea('bride_description',array('class'=>'form-control input-md','rows'=>3))?>
                      </div>
                    </div>
                  </div>
<!--END BRIDES DETAILS--><br>

                  <p class="subtitle fancy"><span>Groom's Details</span></p><br>
									<!--BEGIN GROOMS DETAILS-->
									<div class="row wizard-container">
											<div class="col-sm-4 col-sm-onset-1 wizard-card">
												 <div class="picture-container">
															<div class="picture">
																<?php
																if(empty($website['Website']['groom_pix'])){
																	$groom_pix = $this->webroot."img/default_groom.png";
																} else {
																	$groom_pix = $this->webroot."bride_groom/groom/".$website['Website']['groom_pix'];
																}
																?>
																	<img src="<?php echo $groom_pix;?>" class="picture-src" id="wizardPicturePreviewGroom" title=""/>
																	<?php echo $this->Form->input('groom_pix',array('type'=>'file',"accept"=>"image/*"))?>
															</div>
															<h6>Choose Groom's Picture</h6>
													</div>
											</div>
											<div class="col-sm-7">
													<div class="form-group">
														<label>First Name <small class="error-message">(required)</small></label>
														<?php echo $this->Form->input('groom_first_name',array('class'=>'form-control input-md'))?>
													</div>
													<div class="form-group">
														<label>Last Name <small class="error-message">(required)</small></label>
														<?php echo $this->Form->input('groom_last_name',array('class'=>'form-control input-md'))?>
													</div>
											</div>
									</div>
									<br>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Describe the Groom</label>
												<?php echo $this->Form->textarea('groom_description',array('class'=>'form-control input-md','rows'=>3))?>
                      </div>
                    </div>
                  </div><br>
									<!--END GROOMS DETAILS-->

									<div class="multi-field-wrapper">
						        <p class="subtitle fancy"><span>Edit Venue Details, Date and Time</span></p>
						        <div class="multi-fields">

                      <p>
                        <br>
                        <?php if (!empty($website['Address'])) :?>
                          <?php foreach ($website['Address'] as $key=>$address) :?>
                            <ul class="row multi-field" style="border-bottom:1px dashed #0ea29d;margin-top:10px">
                              <div class="form-group">
                                <div class="col-md-12">
                                  <div class="row">
                                    <?php
                                    $event_types = array(
                                      'Engagement'=>'Traditional Wedding',
                                      'White'=>'White Wedding',
                                      'Nikkai'=>'Nikkai',
                                      'Reception'=>'Reception'
                                    );
                                    ?>
                                    <div class="col-sm-3">
                                      <?php echo $this->Form->hidden("Address.{$key}.id") ?>
                                      <?php echo $this->Form->input("Address.{$key}.name",array('required'=>true,'class'=>'form-control input-md','options'=>$event_types,'empty'=>'--Event Type--')) ?>
                                    </div>
                                    <br class="visible-xs">
                                    <div class="col-sm-3">
                                      <?php echo $this->Form->input("Address.{$key}.date",array('required'=>true,'class'=>'form-control input-md cloned-input','placeholder'=>'--Event Date--','readonly'=>true)) ?>
                                    </div>
                                    <br class="visible-xs">
                                    <div class="col-sm-3">
                                      <div class="input-group bootstrap-timepicker timepicker pull-right">
                                        <?php echo $this->Form->input("Address.{$key}.time",array('required'=>true,'class'=>'form-control input-md time-picker-new','id'=>'time1')) ?>
                                      </div>
                                    </div>
                                    <br class="visible-xs">
                                    <div class="col-sm-3">
                                      <?php echo $this->Form->input("Address.{$key}.timezone",array('required'=>true,'class'=>'form-control input-md','options'=>$this->Timezone->timezone_list(),'empty'=>'--Event Timezone--')) ?>
                                    </div>
                                    <br class="visible-xs">
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <div class="row">
                                    <div class="col-sm-6">
                                      <?php echo $this->Form->input("Address.{$key}.street",array('required'=>true,'class'=>'form-control input-md','placeholder'=>'Street Name, Building, Landmark')) ?>
                                    </div>
                                    <br class="visible-xs">
                                    <div class="col-sm-3">
                                      <?php echo $this->Form->input("Address.{$key}.city",array('required'=>true,'class'=>'form-control input-md','placeholder'=>'City')) ?>
                                    </div>
                                    <br class="visible-xs">
                                    <div class="col-sm-3">
                                      <?php echo $this->Form->input("Address.{$key}.country",array('required'=>true,'class'=>'form-control input-md','options'=>$this->Country->countrylist(),'empty'=>'--Country--')) ?>
                                    </div>
                                    <br class="visible-xs">
                                  </div>
                                </div>
                              </div>
                              <div>
                                <?php if(!empty($address['id'])):?>

                                <a id="<?php echo $address['id']?>" style="margin-top:5px;margin-bottom:10px" class="remove pull-right btn btn-xs btn-danger delete-link" href="#">
                                  <span><i class="fa fa-remove"></i> Remove this address</span>
                                </a>

                              <?php else:?>
                                <a style="margin-top:5px;margin-bottom:10px" class="remove pull-right btn btn-xs btn-danger" href="#">
                                    <span><i class="fa fa-remove"></i> Remove this address</span>
                                </a>

                              <?php endif;?>
                              </div>
                            </ul>
                          <?php endforeach;?>
                        <?php endif;?>
                      </p>


                      <script type="text/javascript" src="<?php echo $this->webroot."js/underscore-min.js" ;?>"></script>
						          <script type="text/javascript" src="<?php echo $this->webroot."js/jquery.js" ;?>"></script>
						          <p>
												<br>
												<script id="grade-template" type="text/x-underscore-template">
						                  <?php echo $this->element('address_edit');?>
						              </script>
						            <table id="grade-table" class="table table-striped table-vcenter">
						              <tbody>
						              </tbody>
						              <tfoot>
						                  <tr>
						                      <td class="actions">
						                        <a style="margin-left: -7px;" class="add btn btn-xs btn-blue" href="#">
						                            <span><i class="fa fa-plus"></i> Add another address</span>
						                        </a>
						                      </td>
						                  </tr>
						              </tfoot>
						          </table>
						          </p>


						        </div>
						      </div>


                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-8">
                        <span class="remember-box checkbox" style="margin-top: 20px;">
                          <label style="padding-left: 0px">
                            Already have a website? <a href="/signup">Promote it Now.</a>
                          </label>
                        </span>
                      </div>
                      <div class="col-md-4">
                        <input type="submit" value="Submit" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                      </div>
                    </div>
                  </div>


                <?php echo $this->Form->end();?>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function(){
  $(".delete-link").click(function()
  {
    var id = $(this).attr("id");
    var link = '<?php echo $this->webroot."addresses/delete"?>';
    var del_id = id;
    var parent = $(this).parent("div").parent("ul");
    if(confirm('Are you sure you want to delete address with ID no = ' +del_id))
    {
      $.post(link, {'del_id':del_id}, function(data)
      {
        parent.fadeOut('slow');
      });
    }
    return false;
  });
});
</script>



<script>
$(document).ready(function() {
    var
        gradeTable = $('#grade-table'),
				newGradeBody = $('.multi-field-wrapper'),
        gradeBody = gradeTable.find('tbody'),
        gradeTemplate = _.template($('#grade-template').remove().text()),
        numberRows = gradeTable.find('.multi-field').length;

    gradeTable
        .on('click', 'a.add', function(e) {
            e.preventDefault();

            if ($('.multi-field', newGradeBody).length < 4)

            $(gradeTemplate({key: numberRows++}))
                .hide()
                .appendTo(gradeBody)
                .fadeIn('fast');

            //TO REMOVE THE ID AND OTHERS FROM DATEPICKER
            $('.multi-field').find('.cloned-input').datepicker("destroy").removeAttr("id").datepicker();

            $input = $('.cloned-input');
            // remove still present related DOM objects
            $input.siblings('.ui-datepicker-trigger,.ui-datepicker-apply').remove();
            // remove datepicker object and detach events
            $input.removeClass('hasDatepicker')
            .removeData('datepicker')
            .unbind()
            .datepicker({
              minDate: -0,
              dateFormat: 'd MM, yy'
            }).attr('readonly','readonly');
            //END DATEPICKER

        })
        .on('click', 'a.remove', function(e) {
            e.preventDefault();

            if ($('.multi-field', gradeBody).length > 1)
              $(this)
                .closest('.multi-field')
                .fadeOut('fast', function() {
                  $(this).remove();
                });
        });

        if (numberRows === 0) {
            gradeTable.find('a.add').click();
        }
});
</script>



<script type="text/javascript">
$(document).ready(function(){
	// Prepare the preview for profile picture
	$("#WebsiteBridePix").change(function(){
		var preview_id = '#wizardPicturePreviewBride';
		readURL(this,preview_id);
	});

	$("#WebsiteGroomPix").change(function(){
		var preview_id = '#wizardPicturePreviewGroom';
		readURL(this,preview_id);
	});

});
function readURL(input,preview_id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(preview_id).attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}
</script>

<script type="text/javascript">
if (typeof jQuery !== 'undefined') {
    jQuery.noConflict();
}
</script>

<?php echo $this->element('validationscript');?>
<script>
$(function(){

	jQuery.validator.addMethod("lettersonly", function(value, element) {
		return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Letters only please");

	jQuery.validator.addMethod("noSpace", function(value, element) {
		return value.indexOf(" ") < 0 && value != "";
	}, "Empty space is not allowed");

	jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
	}, "Only letters and numbers are allowed");

  jQuery.validator.addMethod("timeyi", function(value, element) {
    return this.optional(element) || /^(1[012]|0[1-9]):[0-5][0-9](\\s)? (am|pm)+$/i.test(value);
	}, "Invalid Time");

  jQuery.validator.addClassRules("timelyz", {
    required: true,
    timeyi: true
  });
  
  jQuery('#WebsiteEditWebsiteForm').validate({
    rules: {
        "data[Website][bride_first_name]": {
            required:true
        },
        "data[Address][time]":{
          required:true,
          timeyi:true
        }
    },
    messages:{
      "data[Website][bride_first_name]":{
        "required":"This field is required"
      },
      "data[Address][time]":{
        "required":"This field is required",
        "timeyi":"Invalid time"
      }
    }
  });

  /*jQuery('.time-picker-new').each(function() {
      jQuery(this).rules('add', {
          required: true,
          timeyi: true,
          messages: {
              required:  "This field is required",
              timeyi:  "Invalid Time"
          }
      });
  });*/

});
</script>
