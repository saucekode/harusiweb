<?php $current_website = $this->Session->read('current_website');?>

<ul class="sidebar-nav-v1 list-group margin-bottom-40">
    <li class="list-group-item">
        <a href="<?php echo $this->webroot."dashboard"?>">
          <i class="fa fa-dashboard" style="color:#444"></i> <span>Dashboard</span>
        </a>
    </li>

    <?php if(!empty($current_website)):?>
    <li class="list-group-item">
        <a href="<?php echo $this->webroot."websites/edit_website/".$current_website['Website']['id']."/".$current_website['Website']['unique_code']?>">
          <i class="fa fa-bar-chart-o" style="color:#444"></i> <span>Edit Website</span>
        </a>
    </li>
  <?php endif;?>

    <li class="list-group-item">
        <a href="<?php echo $this->webroot."albums/my_album"?>">
          <i class="fa fa-photo" style="color:#444"></i> <span>Manage Photo Album</span>
        </a>
    </li>
    <li class="list-group-item ">
      <span class="badge badge-u btn-danger">New</span>
      <a href="#">
        <i class="fa fa-users" style="color:#444"></i>
        <span>Manage Invites</span>
      </a>
    </li>
    <li class="list-group-item">
        <a href="#"><i class="fa fa-envelope-o" style="color:#444"></i> <span>SMS Settings</span></a>
    </li>
    <li class="list-group-item ">
      <span class="badge badge-u btn-danger">New</span>
      <a href="#">
        <i class="fa fa-money" style="color:#444"></i>
        <span>Donations</span>
      </a>
    </li>
    <hr>
    <li class="list-group-item">
        <a href="#"><i class="fa fa-book" style="color:#444"></i> <span>My Wedding Blog</span></a>
    </li>
    <li class="list-group-item">
        <a href="#"><i class="fa fa-cogs" style="color:#444"></i> <span>Account Settings</span></a>
    </li>
</ul>
