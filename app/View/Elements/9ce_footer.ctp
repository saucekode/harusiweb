<!-- FOOTER
============================================= -->
<footer id="footer">
  <div class="container">


    <!-- FOOTER LINKS -->
    <div class="row">

      <div id="footer_socials" class="col-sm-12">
        <ul class="contact-socials clearfix">
          <li><a class="foo_link" href="index.html#">Privacy Policy</a></li>
          <li><a class="foo_link" href="index.html#">Terms of Service</a></li>
          <li><a class="foo_link" href="index.html#">Blog</a></li>
          <li><a class="foo_link" href="index.html#">Facebook</a></li>
          <li><a class="foo_link" href="index.html#">Twitter</a></li>
          <li><a class="foo_link" href="index.html#">Instagram</a></li>
          <li><a class="foo_link" href="index.html#">Behance</a></li>
        </ul>
      </div>

    </div>	 <!-- END FOOTER LINKS -->


    <!-- FOOTER COPYRIGHT -->
    <div class="row">

      <div id="footer_copyright" class="col-sm-12">
        <p>Copyright <?php echo date('Y');?> <span>Beldore Technologies</span>. All Rights Reserved.</p>
      </div>

    </div>	 <!-- END FOOTER COPYRIGHT -->


  </div>	   <!-- End container -->
</footer>	<!-- END FOOTER -->
