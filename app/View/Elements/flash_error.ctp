<div class="alert alert-mini alert-danger" id="mydiv">
    <?php echo $message;?>
</div>
<script type="text/javascript">
setTimeout(function() {
    $('#mydiv').fadeOut('fast');
}, 5000);
</script>
