<style type="text/css">
@import "<?php echo $this->webroot."9ce/css/dashboard.css"?>";
</style>

<div class="row">
  <div class="col-lg-6 col-sm-6 col-xs-6">
      <div class="card">
          <div class="content">
              <div class="row">
                  <div class="col-xs-5">
                      <div class="icon-big icon-warning text-center">
                          <i class="fa fa-envelope"></i>
                      </div>
                  </div>
                  <div class="col-xs-7">
                      <div class="numbers">
                          <p>SMS</p>
                          0
                      </div>
                  </div>
              </div>
              <div class="footer">
                  <hr>
                  <div class="stats">
                      <i class="fa fa-refresh"></i> Updated now
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="col-lg-6 col-sm-6 col-xs-6">
      <div class="card">
          <div class="content">
              <div class="row">
                  <div class="col-xs-4">
                      <div class="icon-big icon-success text-center">
                          <i class="fa fa-money"></i>
                      </div>
                  </div>
                  <div class="col-xs-8">
                      <div class="numbers">
                          <p>Donations</p>
                          $0
                      </div>
                  </div>
              </div>
              <div class="footer">
                  <hr>
                  <div class="stats">
                      <i class="fa fa-calendar"></i> Last hour
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="col-lg-6 col-sm-6 col-xs-6">
      <div class="card">
          <div class="content">
              <div class="row">
                  <div class="col-xs-5">
                      <div class="icon-big icon-danger text-center">
                          <i class=" fa fa-image"></i>
                      </div>
                  </div>
                  <div class="col-xs-7">
                      <div class="numbers">
                          <p>Images</p>
                          0/~
                      </div>
                  </div>
              </div>
              <div class="footer">
                  <hr>
                  <div class="stats">
                      <i class="fa fa-clock-o"></i> In the last hour
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="col-lg-6 col-sm-6 col-xs-6">
      <div class="card">
          <div class="content">
              <div class="row">
                  <div class="col-xs-5">
                      <div class="icon-big icon-info text-center">
                          <i class="fa fa-ticket"></i>
                      </div>
                  </div>
                  <div class="col-xs-7">
                      <div class="numbers">
                          <p>Invites</p>
                          0
                      </div>
                  </div>
              </div>
              <div class="footer">
                  <hr>
                  <div class="stats">
                      <i class="fa fa-refresh"></i> Updated now
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
