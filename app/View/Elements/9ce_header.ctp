<header id="header">
  <div class="navbar navbar-fixed-top">
    <div class="container">


      <!-- Navigation Bar -->
      <div class="navbar-header">

        <!-- Responsive Menu Button -->
        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-menu">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <!-- Logo Image -->
        <a id="GoToHome" class="navbar-brand" href="<?php echo $this->webroot;?>"><img src="<?php echo $this->webroot."9ce/"?>img/logo.png" alt="logo"></a>

      </div>	<!-- End Navigation Bar -->


      <!-- Navigation Menu -->
      <nav id="navigation-menu" class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">

          <li><a href="#">About</a></li>
          <li><a href="#">How it Works</a></li>
          <?php $check_user = $this->requestAction('/users/checklogin');?>

          <?php if($check_user){?>

            <?php
            $user_id = $this->Session->read('Auth.User.id');
            $website_id = $this->Session->read('Auth.User.Website.id');
            $website_unique_code = $this->Session->read('Auth.User.Website.unique_code');
            ?>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Menu <span class="caret"></span></a>
              <ul class="dropdown-menu">

                <li>
                  <a href="<?php echo $this->webroot."dashboard";?>">
                    Dashboard
                  </a>
                </li>

                <li>
                  <a href="<?php echo $this->webroot."websites/edit_website/".$website_id."/".$website_unique_code;?>">
                    Edit Website
                  </a>
                </li>

                <li>
                  <a href="<?php echo $this->webroot."users/account_settings";?>">
                    Edit Profile
                  </a>
                </li>

                <li>
                  <a href="<?php echo $this->webroot."logout"?>">
                    Logout
                  </a>
                </li>
                
              </ul>
            </li>

          <?php } else { ?>

            <li><a href="<?php echo $this->webroot."login"?>">Log In</a></li>
            <li><a href="#">Sign Up</a></li>

          <?php } ?>

        </ul>
      </nav>  <!-- End Navigation Menu -->


    </div>	  <!-- End container -->
  </div>	   <!-- End navbar fixed top  -->
</header>	<!-- END HEADER -->
