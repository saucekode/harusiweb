<?php
$key = isset($key) ? $key : '<%= key %>';
?>
<tr>
    <td>
        <?php echo $this->Form->hidden("Dissertationreview.{$key}.id") ?>
        <?php echo $this->Form->text("Dissertationreview.{$key}.url", array('class'=>'col-sm-12')); ?>
        <input name="date" class="form-control input-md date venue_date" type="text" data-date="" data-date-format="dd MM yyyy hh:ii" data-link-format="yyyy-mm-dd hh:ii" readonly="true" placeholder="Date and Time">


    </td>
    <td class="actions">
        <a href="#" class="remove">Remove</a>
    </td>
</tr>
