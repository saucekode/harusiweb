<?php
$subscriptions = $this->requestAction('/subscriptions/get_subscriptions');
$naira = "&#8358;";
$website_id = $this->Session->read('Auth.User.Website.id');
?>

<?php if(!empty($subscriptions)){?>

<div class="row">
<div class="pricing-table">

<?php foreach ($subscriptions as $key => $subscription):?>

<div class="col-md-3 col-sm-6 <?php echo $subscription['Subscription']['name']=='Standard' ? 'center':'';?>">

<div class="plan <?php echo $subscription['Subscription']['name']=='Standard' ? 'most-popular':'';?>">

<?php if($subscription['Subscription']['name']=='Standard'):?>
  <div class="plan-ribbon-wrapper"><div class="plan-ribbon">Popular</div></div>
<?php endif;?>

  <h3>
    <?php echo $subscription['Subscription']['name'];?>
    <?php if($subscription['Subscription']['price'] == 0):?>
      <span>FREE</span>
    <?php else:?>
      <span><?echo $naira.number_format($subscription['Subscription']['price']);?></span>
    <?php endif;?>
  </h3>

  <?php
  $id = $subscription['Subscription']['id'];
  $name = $subscription['Subscription']['name'];
  $subscribe_link = $this->Form->postLink(__("Subscribe"), array('controller'=>'subscriptions', 'action' => 'activate_sub', $id, $website_id), array('class'=>'btn btn-lg btn-primary'), __("Are you sure you want to subscribe to the ".$name." Plan?"));
   ?>

  <?php echo $subscribe_link;?>


  <ul>
    <li><strong><?php echo $subscription['Subscription']['sms'];?></strong> SMS</li>

    <li>
      <strong>
        <?php
        if($subscription['Subscription']['no_of_images'] == 0):
          echo 'Unlimited';
        else:
          echo $subscription['Subscription']['no_of_images'];
        endif;
        ?>
      </strong>
       Photos
     </li>

     <li>
       <strong>
         <?php
         if($subscription['Subscription']['no_of_albums'] == 0):
           echo 'Unlimited';
         else:
           echo $subscription['Subscription']['no_of_albums'];
         endif;
         ?>
       </strong>
       Albums
      </li>

      <li>
        <strong>
          <?php
          if($subscription['Subscription']['blog_posts'] == 0):
            echo 'Unlimited';
          else:
            echo $subscription['Subscription']['blog_posts'];
          endif;
          ?>
        </strong>
        Blog Posts
       </li>

       <li><strong>Unlimited</strong> Invites and Donations</li>

       <li>
         <strong>
           <?php
           echo $subscription['Subscription']['donation_percent']."%";
           ?>
         </strong>
         on Donations Withdrawal
       </li>
       <li></li>

  </ul>

  <?php
$subscribe_link = $this->Form->postLink(__("Subscribe"), array('controller'=>'subscriptions', 'action' => 'activate_sub', $id, $website_id), array('class'=>'btn btn-lg btn-primary'), __("Are you sure you want to subscribe to the ".$name." Plan?"));
   ?>
  <?php echo $subscribe_link;?>
</div>
</div>

<?php endforeach;?>

</div>
</div>

<?php }?>
