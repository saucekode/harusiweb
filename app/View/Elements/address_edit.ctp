<?php
$key = isset($key) ? $key : '<%= key %>';
?>
<ul class="row multi-field" style="border-bottom:1px dashed #0ea29d;margin-top:10px">
  <div class="form-group">
    <div class="col-md-12">
      <div class="row">
        <?php
        $event_types = array(
          'Engagement'=>'Traditional Wedding',
          'White'=>'White Wedding',
          'Nikkai'=>'Nikkai',
          'Reception'=>'Reception'
        );
        ?>
        <div class="col-sm-3">
          <?php echo $this->Form->input("sfsf.{$key}.name",array('required'=>true,'class'=>'form-control input-md','options'=>$event_types,'empty'=>'--Event Type--')) ?>
        </div>
        <br class="visible-xs">
        <div class="col-sm-3">
          <?php echo $this->Form->input("sfsf.{$key}.date",array('required'=>true,'class'=>'form-control input-md cloned-input','placeholder'=>'--Event Date--','readonly'=>true)) ?>
        </div>
        <br class="visible-xs">
        <div class="col-sm-3">
          <div class="input-group bootstrap-timepicker timepicker pull-right">
            <?php echo $this->Form->input("sfsf.{$key}.time",array('class'=>'form-control input-md timelyz','placeholder'=>'12:05 PM')) ?>
          </div>
        </div>
        <br class="visible-xs">
        <div class="col-sm-3">
          <?php echo $this->Form->input("sfsf.{$key}.timezone",array('required'=>true,'class'=>'form-control input-md','options'=>$this->Timezone->timezone_list(),'empty'=>'--Event Timezone--')) ?>
        </div>
        <br class="visible-xs">
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-md-12">
      <div class="row">
        <div class="col-sm-6">
          <?php echo $this->Form->input("sfsf.{$key}.street",array('required'=>true,'class'=>'form-control input-md','placeholder'=>'Street Name, Building, Landmark')) ?>
        </div>
        <br class="visible-xs">
        <div class="col-sm-3">
          <?php echo $this->Form->input("sfsf.{$key}.city",array('required'=>true,'class'=>'form-control input-md','placeholder'=>'City')) ?>
        </div>
        <br class="visible-xs">
        <div class="col-sm-3">
          <?php echo $this->Form->input("sfsf.{$key}.country",array('required'=>true,'class'=>'form-control input-md','options'=>$this->Country->countrylist(),'empty'=>'--Country--')) ?>
        </div>
        <br class="visible-xs">
      </div>
    </div>
  </div>
  <div>
    <a style="margin-top:5px;margin-bottom:10px" class="remove pull-right btn btn-xs btn-danger" href="#">
        <span><i class="fa fa-remove"></i> Remove this address</span>
    </a>
  </div>
</ul>
