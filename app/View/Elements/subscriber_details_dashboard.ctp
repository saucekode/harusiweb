<style type="text/css">
@import "<?php echo $this->webroot."9ce/css/dashboard.css"?>";
</style>

<?php
$website_id = $this->Session->read('Auth.User.Website.id');
$sub_details = $this->requestAction('/subscribers/get_subscriber_details/'.$website_id);

$website_sms = $sub_details['Subscriber']['sms'];
$website_images = $sub_details['Subscriber']['no_of_images'];

$subscription_sms = $sub_details['Subscription']['sms'];
$subscription_images = $sub_details['Subscription']['no_of_images'];
?>


<div class="row">
  <div class="col-lg-6 col-sm-6 col-xs-6">
      <div class="card">
          <div class="content">
              <div class="row">
                  <div class="col-xs-5">
                      <div class="icon-big icon-warning text-center">
                          <i class="fa fa-envelope"></i>
                      </div>
                  </div>
                  <div class="col-xs-7">
                      <div class="numbers">
                          <p>SMS</p>
                          <?php echo ($subscription_sms-$website_sms)."/".$subscription_sms;?>
                      </div>
                  </div>
              </div>
              <div class="footer">
                  <hr>
                  <div class="stats">
                      <i class="fa fa-refresh"></i> Updated now
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="col-lg-6 col-sm-6 col-xs-6">
      <div class="card">
          <div class="content">
              <div class="row">
                  <div class="col-xs-4">
                      <div class="icon-big icon-success text-center">
                          <i class="fa fa-money"></i>
                      </div>
                  </div>
                  <div class="col-xs-8">
                      <div class="numbers">
                          <p>Donations</p>
                          <?php $website_donations = $this->requestAction('donations/sum_website_donations/'.$website_id);?>
                          $<?php echo $website_donations;?>
                      </div>
                  </div>
              </div>
              <div class="footer">
                  <hr>
                  <div class="stats">
                      <i class="fa fa-calendar"></i> Last hour
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="col-lg-6 col-sm-6 col-xs-6">
      <div class="card">
          <div class="content">
              <div class="row">
                  <div class="col-xs-5">
                      <div class="icon-big icon-danger text-center">
                          <i class=" fa fa-image"></i>
                      </div>
                  </div>
                  <div class="col-xs-7">
                      <div class="numbers">
                          <p>Images</p>
                          <?php if($sub_details['Subscription']['no_of_images'] == 0):?>

                          <?php $count_website_images = $this->requestAction('albumfiles/count_website_images/'.$website_id);?>

                            <?php echo ($count_website_images)."/~";?>

                          <?php else:?>

                            <?php echo ($subscription_images-$website_images)."/".$subscription_images;?>

                          <?php endif;?>
                      </div>
                  </div>
              </div>
              <div class="footer">
                  <hr>
                  <div class="stats">
                      <i class="fa fa-clock-o"></i> In the last hour
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="col-lg-6 col-sm-6 col-xs-6">
      <div class="card">
          <div class="content">
              <div class="row">
                  <div class="col-xs-5">
                      <div class="icon-big icon-info text-center">
                          <i class="fa fa-ticket"></i>
                      </div>
                  </div>
                  <div class="col-xs-7">
                      <div class="numbers">
                          <p>Invites</p>
                          <?php $count_website_invites = $this->requestAction('invites/count_website_invites/'.$website_id);?>

                          <?php echo "+".($count_website_invites);?>
                      </div>
                  </div>
              </div>
              <div class="footer">
                  <hr>
                  <div class="stats">
                      <i class="fa fa-refresh"></i> Updated now
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
