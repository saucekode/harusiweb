<?php $this->set('title_for_layout','Login to manage your website');?>

<section style="padding-top:40px;padding-bottom:20px;">
	<div class="container">


		<!-- SECTION TITLE -->
		<div class="row">
			<div class="col-sm-12 titlebar" style="margin-bottom:0px">
        <?php echo $this->Session->flash();?>

				<h3>Login</h3>
				<p>Please login to access your website </p>

			</div>
		</div>	<!-- END SECTION TITLE -->

    <div class="row">
      <div class="col-md-12">
        <div class="featured-boxes">
          <div class="row">
            <div class="col-sm-6 col-md-offset-3 col-md-onset-3">
              <div class="featured-box featured-box-primary align-left mt-xlg" style="margin-top:15px">
                <div class="box-content">
                  <form action="<?php echo $this->here;?>" id="UserLoginForm" method="post" accept-charset="utf-8" novalidate="novalidate" class="form-horizontal">
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-12">
                          <label>E-mail Address</label>
                          <input name="data[User][username]" class="form-control input-lg" type="text" id="UserUsername">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-12">
                          <a class="pull-right" href="/forgot_password">(Forgot Your Password?)</a>
                          <label>Password</label>
                          <input name="data[User][password]" class="form-control input-lg" type="password" id="UserPassword" required="required" aria-required="true">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                        <div class="col-md-8">
                          <span class="remember-box checkbox" style="margin-top: 20px;">
                            <label style="padding-left: 0px">
                              Don't have an account? <a href="/signup">Register Now.</a>
                            </label>
                          </span>
                        </div>
                        <div class="col-md-4">
                          <input type="submit" value="Login" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



	</div>	    <!-- End container -->
</section>
