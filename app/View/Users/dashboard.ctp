<?php
$this->set('title_for_layout','Dashboard');
$user_website = $this->Session->read('Auth.User.Website.id');
$user_website_name = $this->Session->read('Auth.User.Website.short_url');
$user_email = $this->Session->read('Auth.User.email');
$user_phone = $this->Session->read('Auth.User.phone');
$user_name = $this->Session->read('Auth.User.name');
$user_last_login = $this->Session->read('Auth.User.last_login');
?>

<section style="padding-top:30px;padding-bottom:20px;">
	<div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="featured-boxes">
          <div class="featured-box featured-box-primary align-left" style="margin-top:0px;">
            <div class="box-content">
              <?php echo $this->element('user_leftbar');?>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-9 col-sm-8 col-xs-12">

				<div class="featured-box featured-box-primary align-left mt-xlg" style="margin-top:0px;background:#F5F5F5">
					<div class="box-content">

        <?php echo $this->Session->flash();?>

        <div class="profile-body">
          <div id="schome" class="scs">
            <div class="row margin-bottom-10">
              <div class="col-sm-12 col-md-6 sm-margin-bottom-20">
                <div class="service-block-v3 service-block-u">
                  <div class="row margin-bottom-10 service-in">

                    <div class="col-xs-6">
                      <h5>Name</h5>
                      <small><?php echo $user_name;?></small>
                    </div>

                    <div class="col-xs-6">
                      <h5>Email</h5>
                      <small><?php echo $user_email;?></small>
                    </div>

              			<div class="col-xs-6" style="margin-top:20px">
              				<h5>Mobile</h5>
              				<small><?php echo $user_phone;?></small>
              			</div>

              			<div class="col-xs-6" style="margin-top:20px">
              				<h5>Last login</h5>
              				<small><?php echo $user_last_login;?></small>
              			</div>

              			<div class="col-xs-6" style="margin-top:18px">
              				<h5>Language</h5>
              				<small>English</small>
              			</div>

                    <div class="col-xs-6" style="margin-top:18px">
              				<h5>Website</h5>
											<?php if(!empty($user_website_name)):?>
												<small><a href="#"><?php echo $user_website_name;?>.harusiweb.com</a></small>
											<?php else:?>
												<small>None. <a href="#">Create your website.</a></small>
											<?php endif;?>

              			</div>

                  </div>
                </div>
              </div>

            <div class="col-md-6 col-sm-12">

							<?php if(!empty($user_website_name)):?>
								<?php echo $this->element('subscriber_details_dashboard');?>
							<?php else:?>
								<?php echo $this->element('subscriber_details_empty');?>
							<?php endif;?>

            	<!--div class="row hidden-xs hidden-sm">
            		<div class="col-lg-4 col-md-12" style="margin-bottom:10px">
            			<button class="btn-u btn-u-lg btn-brd btn-brd-hover btn-block doaction" style="padding:10px 0;text-align:center;"type="button"><i class="fa fa-envelope text-danger"></i><br> SMS</button>
            		</div>
            		<div class="col-lg-4 col-md-12" style="margin-bottom:10px">
            			<button class="btn-u btn-u-lg btn-brd btn-brd-hover btn-block doaction" style="padding:10px 0;text-align:center;" type="button"><i class="fa fa-usd text-danger"></i><br><span>DONATIONS</span></button>
            		</div>
            		<div class="col-lg-4 col-md-12" style="margin-bottom:10px">
            			<button class="btn-u btn-u-lg btn-brd btn-brd-hover btn-block doaction" style="padding:10px 0;text-align:center;" type="button"><i class="fa fa-bars text-danger"></i><br> <span>INVITES</span></button>
            		</div>
              </div-->

            </div>
          </div>

          <hr>
        </div>
      </div>

      <!--div class="row">

				<!--get 5 invites on the dashboard;-->
				<!--div class="col-md-6">
          <h5>Invites</h5>

					<?php
					if(!empty($user_website)):
						$get_invites = $this->requestAction('/invites/get_invites/'.$user_website);
					endif;
					?>
					<div class="table-responsive">
						<table class="table table-bordered mb-none">
							<thead>
								<tr>
									<th width="10%">#</th>
									<th width="50%">Name</th>
									<th>Phone Number</th>
								</tr>
							</thead>
							<tbody>

								<?php
								if(!empty($get_invites)){
									foreach($get_invites as $key => $get_invite):
								?>
									<tr>
										<td><?php echo $key + 1;?></td>
										<td><?php echo $get_invite['Invite']['name'];?></td>
										<td><?php echo $get_invite['Invite']['phone_number'];?></td>
									</tr>

								<?php endforeach; } else { ?>
									<tr>
										<td colspan="3" class="text-center">No invites available</td>
									</tr>
								<?php } ?>

							</tbody>
						</table>
					</div>
					<?php if(!empty($get_invites)):?>
	          <div class="text-center">
	            <a class="btn btn-blue btn-xs" href="#">View more</a>
	          </div>
					<?php endif;?>

				</div>
				<!--End get 5 invites on the dashboard;-->


        <hr class="visible-xs">

				<!--get 5 donations on the dashboard;-->
				<!--div class="col-md-6">
          <h5>Donations</h5>

					<?php
					if(!empty($user_website)):
						$get_donations = $this->requestAction('/donations/get_donations/'.$user_website);
					endif;
					?>
					<div class="table-responsive">
						<table class="table table-bordered mb-none">
							<thead>
								<tr>
									<th width="10%">#</th>
									<th width="60%">Name</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>

								<?php
								if(!empty($get_donations)){
									foreach($get_donations as $key => $get_donation):
								?>
									<tr>
										<td><?php echo $key + 1;?></td>
										<td>
											<?php
											if($get_donation['Donation']['anonymous'] == 1):
												echo 'Anonymous';
											else:
												echo $get_donation['Donation']['name'];
											endif;
											?>
										</td>
										<td><?php echo $get_donation['Donation']['price'];?></td>
									</tr>

								<?php endforeach; } else { ?>
									<tr>
										<td colspan="3" class="text-center">No donations available</td>
									</tr>
								<?php } ?>

							</tbody>
						</table>
					</div>

					<?php if(!empty($get_donations)):?>
	          <div class="text-center">
	            <a class="btn btn-blue btn-xs" href="#">View more</a>
	          </div>
					<?php endif;?>
				</div>
				<!--End get 5 donations on the dashboard;-->

			<!--/div-->


		</div>
	</div>
    </div>
  </div>
</div>
</section>
