<?php $this->set('title_for_layout','Account Settings');?>

<style type="text/css">
@import url(<?php echo $this->webroot."9ce/css/fancy-lines.css"?>);
</style>

<section style="padding-top:30px;padding-bottom:20px;">
	<div class="container">

    <!-- SECTION TITLE -->
		<div class="row">
			<div class="col-sm-12 titlebar" style="margin-bottom:0px">
        <?php echo $this->Session->flash();?>

				<h3>Account Settings</h3><p></p>

			</div>
		</div>	<!-- END SECTION TITLE -->

    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="featured-boxes">
          <div class="featured-box featured-box-primary align-left" style="margin-top:0px;">
            <div class="box-content">
              <?php echo $this->element('user_leftbar');?>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-9 col-sm-8 col-xs-12">
        <?php echo $this->Session->flash();?>

        <div class="row">
          <div class="col-sm-10 col-md-onset-2">
            <div class="featured-box featured-box-primary align-left mt-xlg" style="margin-top:0px;background:#F5F5F5">
              <div class="box-content">
                <?php echo $this->Form->create('User', array('class'=>'form-horizontal','inputDefaults' => array('label' => false, 'div' => false))); ?>

                  <p class="subtitle fancy"><span>Edit Your Name and Email address</span></p>
                  <br>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Full Name</label>
												<?php echo $this->Form->input('name',array('class'=>'form-control input-md'))?>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-sm-6">
                            <label>Email address</label>
                            <?php echo $this->Form->input('dshdsds',array('class'=>'form-control input-md','value'=>$user['User']['email'],'disabled'=>true))?>
                            <br class="visible-xs">
                          </div>
                          <div class="col-sm-6">
                            <label>Phone Number</label>
                            <?php echo $this->Form->input('phone',array('class'=>'form-control input-md'))?>
                            <br class="visible-xs">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <br>
                  <p class="subtitle fancy"><span>Password Change</span></p>
                  <br>
                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-sm-6">
                            <label>New Password</label>
														<?php echo $this->Form->input('password_update',array('class'=>'form-control input-md','type'=>'password'))?>
                            <br class="visible-xs">
                          </div>
                          <div class="col-sm-6">
                            <label>Confirm New Password</label>
                            <?php echo $this->Form->input('password_confirm_update',array('class'=>'form-control input-md','type'=>'password'))?>
                            <br class="visible-xs">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <div class="col-md-4 col-md-offset-4 col-md-onset-4 text-center">
                        <input type="submit" value="Submit" class="btn btn-primary mb-xl" data-loading-text="Loading...">
                      </div>
                    </div>
                  </div>


                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
