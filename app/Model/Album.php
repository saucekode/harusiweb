<?php

App::uses('AppModel', 'Model');

class Album extends AppModel {
    var $belongsTo = array('Website');
    var $hasMany = array('AlbumFile');
}
?>
