<?php

App::uses('AppModel', 'Model');

class Subscriber extends AppModel {
    var $belongsTo = array('Subscription', 'Website');
}
?>
