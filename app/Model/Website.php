<?php

App::uses('AppModel', 'Model');

class Website extends AppModel {

    public $validate = array(
        'short_url' => array(
            'notEmpty'=> array(
                'rule'=>array('notEmpty'),
                'message'=>'Please provide your short_url'
            )
        )
    );


    var $hasMany = array('Invite','Donation','Album','Address');
    var $belongsTo = array('User','Template');
    var $hasOne = array('Subscriber');
}
?>
