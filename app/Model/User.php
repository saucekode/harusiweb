<?php

App::uses('AppModel', 'Model');

App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
App::uses('Security', 'Utility');

class User extends AppModel {

    public $validate = array(
        'name' => array(
            'notEmpty'=> array(
                'rule'=>array('notEmpty'),
                'message'=>'Please provide your full name'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            ),
            'min_length' => array(
                'rule' => array('minLength', '4'),
                'message' => 'Password must have a mimimum of 4 characters'
            )
        ),

        'password_confirm' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Please confirm your password'
            ),
             'equaltofield' => array(
                'rule' => array('equaltofield','password'),
                'message' => 'Both passwords must match.'
            )
        ),

        'email' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Please provide a valid email address',
                'allowEmpty' => false
            ),
            'isUnique' => array(
                'rule'    => array('isUnique'),
                'message' => 'This email is already in use'
            )
        ),
        'password_update' => array(
            'min_length' => array(
                'rule' => array('minLength', '6'),
                'message' => 'Password must have a mimimum of 6 characters',
                'allowEmpty' => true,
                'required' => false
            )
        ),
        'password_confirm_update' => array(
             'equaltofield' => array(
                'rule' => array('equaltofield','password_update'),
                'message' => 'Both passwords must match.',
                'required' => false,
            )
        )


    );


    var $hasOne = array('Website');
    var $belongsTo = array('UserGroup');

    public function equaltofield($check,$otherfield)
    {
        //get name of field
        $fname = '';
        foreach ($check as $key => $value){
            $fname = $key;
            break;
        }
        return $this->data[$this->name][$otherfield] === $this->data[$this->name][$fname];
    }


     public function beforeSave($options = array()) {
        // hash our password
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }

         if (isset($this->data[$this->alias]['password_update'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password_update']
            );
        }

        return parent::beforeSave($options);
    }

}
?>
